# imas-meishi

## Example can be found in this repo.

Forked from [opieters/business-card](https://github.com/opieters/business-card)

Requirements
------------

* XeLaTeX
* [Font Awesome](https://fontawesome.com/) (Requires version 5 or higher due to Discord icon added in 5.0+)
* [Fira Sans](https://github.com/mozilla/Fira)

Building Documents
------------------

Build the front and back sides with XeLaTeX:

```bash
xelatex business-card.tex
```

SVG Files
---------

LaTeX is not equipped to handle SVG files directly. A conversion to a PDF file is needed. This can be done using an external tool such as Inkscape:

```bash
inkscape --without-gui --export-area-drawing --file=logo.svg --export-pdf=logo.pdf
```

License
-------

Files in `/figures` are licensed by ©BANDAI NAMCO Entertainment Inc. and are free to use as long as they're non-commercial.
https://millionlive.idolmaster.jp/theaterdays/cp/　(Link broken)

The figures are vector-traced and exported as PDF.

Code is licensed under GNU GPLv3. See LICENSE file.

© 2017 Olivier Pieters
© 2023 Harris Anthonio
